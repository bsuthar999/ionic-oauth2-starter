import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, from } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';
import {
  CLIENT_ID,
  ELECTRON_REDIRECT,
  SCOPES,
  AUTHORIZATION_CODE,
} from './constants/storage';
import { StorageService } from './common/services/storage-service/storage.service';
import { OAuth2Service } from './common/services/oauth2/oauth2.service';

@Injectable()
export class AppService {
  codeVerifier: string;
  verifier: any = {
    client_id: this.storageService.getInfo(CLIENT_ID),
    redirect_uri: ELECTRON_REDIRECT,
    scope: SCOPES,
  };
  messageUrl = 'https://staging-admin.castlecraft.in/info'; // URL to web api
  constructor(
    private readonly http: HttpClient,
    private readonly storageService: StorageService,
    private readonly oauth2: OAuth2Service,
  ) {}

  /** GET message from the server */
  getMessage(): Observable<any> {
    return this.http.get<any>(this.messageUrl).pipe(
      switchMap(appInfo => {
        if (appInfo.message) {
          return of(appInfo);
        }
        return this.http.get<any>(appInfo.authServerURL + '/info').pipe(
          map(authInfo => {
            appInfo.services = authInfo.services;
            this.storageService.setInfoLocalStorage(appInfo);
            return appInfo;
          }),
        );
      }),
    );
  }

  oAuthLogin() {
    this.verifier.grant_type = AUTHORIZATION_CODE;

    from(
      this.oauth2.getAuthorizationCode({
        scope: SCOPES,
        additionalAuthCodeRequestData: {
          code_challenge: this.oauth2.generateRandomString(64),
          code_challenge_method: 'plain',
        },
      }),
    ).subscribe({
      next: (response: { code: string; state: any }) => {
        this.verifier.code = response.code;
        this.verifier.state = response.state;
        this.oauth2.getNewToken(this.verifier);
      },
      error: error => {},
    });
  }

  getRefreshToken() {
    // this.verifier.grant_type = REFRESH_TOKEN;
    // this.oauth2.getRefreshToken(this.verifier);
  }
}
