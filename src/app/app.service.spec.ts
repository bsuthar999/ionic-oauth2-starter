import { TestBed, getTestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { AppService } from './app.service';
import { StorageService } from './common/services/storage-service/storage.service';
import { OAuth2Service } from './common/services/oauth2/oauth2.service';

describe('AppService', () => {
  let injector: TestBed;
  let service: AppService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        AppService,
        {
          provide: Storage,
          useValue: {},
        },
        {
          provide: StorageService,
          useValue: {
            getInfo: (...args) => '',
            setInfoLocalStorage: (...args) => {},
          },
        },
        {
          provide: OAuth2Service,
          useValue: {},
        },
      ],
    });

    injector = getTestBed();
    service = injector.get(AppService);
    httpMock = injector.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  afterEach(() => {
    httpMock.verify();
  });
});
