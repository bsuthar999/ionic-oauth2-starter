import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { MatSnackBar } from '@angular/material';
import { AppService } from './app.service';
import { OAuth2Service } from './common/services/oauth2/oauth2.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
})
export class AppComponent implements OnInit {
  loggedIn = true;
  data = 'loading .... ';
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private snackBar: MatSnackBar,
    private appService: AppService,
    private readonly oauth2: OAuth2Service,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    this.data = window.location.origin;
    this.appService.getMessage().subscribe({
      next: response => {
        if (response.message) {
          this.snackBar.open('Server Setup Incomplete', 'Close', {
            duration: 2500,
          });
        }
        this.oauth2.setConfig({
          redirectUri: 'http://localhost',
          clientId: response.clientId,
          scope: 'openid email roles',
          authorizationUrl: response.authorizationURL,
        });
      },
    });
  }

  login() {
    this.appService.oAuthLogin();
    this.loggedIn = false;
  }
  refreshToken() {
    this.data = JSON.stringify(window.location.href);
    // this.appService.getRefreshToken();
  }
}
