import { stringify } from 'querystring';
import { parse } from 'url';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  ACCESS_TOKEN,
  REFRESH_TOKEN,
  ID_TOKEN,
  SCOPE,
  TOKEN_TYPE,
} from '../../../constants/storage';
import { StorageService } from '../storage-service/storage.service';

export interface OAuth2Config {
  redirectUri: string;
  clientId: string;
  scope: string;
  authorizationUrl: string;
}

export interface OAuth2UrlParams {
  response_type?: string;
  redirect_uri?: string;
  client_id?: string;
  state?: string;
  scope?: string;
  access_type?: string;
}

export interface GetCodeOpts {
  scope?: string;
  accessType?: string;
  additionalAuthCodeRequestData?: any;
}

@Injectable()
export class OAuth2Service {
  config: OAuth2Config;
  authorizationHeaders: HttpHeaders;
  tokenRequestUrl = 'https://staging-accounts.castlecraft.in/oauth2/';
  constructor(
    private readonly http: HttpClient,
    private readonly storageService: StorageService,
  ) {}

  setConfig(config: OAuth2Config) {
    this.config = config;
  }

  getAuthorizationCode(opts: GetCodeOpts) {
    opts = opts || {};
    let urlParams: OAuth2UrlParams = {
      response_type: 'code',
      redirect_uri: this.config.redirectUri,
      client_id: this.config.clientId,
      state: this.generateRandomString(16),
    };

    if (opts.scope) {
      urlParams.scope = opts.scope;
    }

    if (opts.accessType) {
      urlParams.access_type = opts.accessType;
    }

    urlParams = Object.assign(urlParams, opts.additionalAuthCodeRequestData);

    const url = this.config.authorizationUrl + '?' + stringify(urlParams);

    return new Promise((resolve, reject) => {
      const authWindow = window.open(url, '_self');
      authWindow.addEventListener('close', onClose => {
        authWindow.close();
      });

      function onCallback(callbackUrl) {
        const urlParts = parse(callbackUrl, true);
        const query = urlParts.query;
        const code = query.code;
        const state = query.state;
        const error = query.error;

        if (error !== undefined) {
          reject(error);
          authWindow.addEventListener('close', () => {
            authWindow.close();
          });
        } else if (code) {
          resolve({ code, state });
          authWindow.close();
        }
      }

      authWindow.addEventListener('popstate', () => {
        onCallback(url);
      });

      authWindow.addEventListener('popstate', () => {
        onCallback(url);
      });
    });
  }

  getNewToken(verifier) {
    this.http.post(this.tokenRequestUrl + 'token', verifier).subscribe({
      next: (response: any) => {
        this.setLocalSecrets(response);
        return this.getToken();
      },
      error: err => {},
    });
  }

  getToken() {
    return this.storageService.getInfo(ACCESS_TOKEN);
  }

  async getRefreshToken(verifier) {
    const oldToken = await this.getToken();
    const refreshToken = this.storageService.getInfo(REFRESH_TOKEN);
    if (refreshToken) {
      verifier.refresh_token = refreshToken;
      this.http.post(this.tokenRequestUrl + 'token', verifier).subscribe({
        next: (data: any) => {
          this.http
            .post(
              this.tokenRequestUrl + 'revoke',
              { token: oldToken },
              { headers: this.getHeaders(data.access_token) },
            )
            .subscribe({
              next: response => {
                this.updateSecrets(data);
                // just to log our ketTar passwords.
              },
              error: err => {},
            });
        },
        error: err => {},
      });
    } else {
      return;
    }
  }

  generateRandomString(length: number) {
    let text = '';
    const possible =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for (let i = 0; i < length; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }

  setLocalSecrets(response) {
    this.storageService.setItem(ACCESS_TOKEN, response.access_token);
    this.storageService.setItem(REFRESH_TOKEN, response.refresh_token);
    this.storageService.setItem(ID_TOKEN, response.id_token);
    this.storageService.setItem(SCOPE, response.scope);
    this.storageService.setItem(TOKEN_TYPE, response.token_type);
  }

  updateSecrets(response) {
    this.storageService.setItem(ACCESS_TOKEN, response.access_token);
    this.storageService.setItem(REFRESH_TOKEN, response.refresh_token);
  }

  getHeaders(token) {
    return (this.authorizationHeaders = new HttpHeaders({
      Authorization: 'Bearer ' + token,
    }));
  }
}
